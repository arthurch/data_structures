#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* this could be wrong (edit it if necessary) */
#include "tree.h"

tree_t *tree_create(void *rootval)
{
  tree_t *tree = (tree_t *) malloc(sizeof(struct tree));
  memset(&(tree->root), 0, sizeof(struct tree_node));

  tree->sz = 1;
  tree->root.owner = tree;
  tree->root.val = rootval;
  return tree;
}

tree_t *tree_init(tree_t *tree, void *rootval)
{
  if(tree->sz != 0)
    tree_empty(tree);

  memset(&(tree->root), 0, sizeof(struct tree_node));

  tree->sz = 1;
  tree->root.owner = tree;
  tree->root.val = rootval;
  return tree;
}

tree_node_t *tree_create_node(void *val)
{
  tree_node_t *n = (tree_node_t *) malloc(sizeof(struct tree_node));
  if(n == NULL)
    return NULL;
  memset(n, 0, sizeof(struct tree_node));
  n->val = val;
  n->flags |= TREE_NODE_TO_BE_FREE;
  return n;
}

tree_node_t *tree_append(tree_node_t *parent, void *val, int allocatedval)
{
  tree_node_t *n = tree_create_node(val);
  if(n == NULL)
    return NULL;

  if(allocatedval)
  {
    n->flags |= TREE_VAL_TO_BE_FREE;
  }

  tree_append_node(parent, n);
  return n;
}

static int tree_children_resize(tree_node_t *n, size_t newsize)
{
  size_t allocated = n->allocated;
  size_t new_allocated;
  size_t allocated_bytes;

  if(allocated >= newsize && newsize >= (allocated >> 1))
  {
    n->len = newsize;
    return n->allocated;
  }

  new_allocated = (size_t) (newsize + ((size_t) newsize >> 3) + 6) & ~(size_t)3;
  if(newsize - n->len > new_allocated - newsize)
    new_allocated = ((size_t) newsize + 3) & ~(size_t)3;

  allocated_bytes = new_allocated * sizeof(tree_node_t *);
 
  tree_node_t **children = (tree_node_t **) realloc(n->children, allocated_bytes);
  if(children == NULL)
  {
    return -1;
  }

  n->children = children;
  n->allocated = new_allocated;
  n->len = newsize;
  return new_allocated;
}

tree_node_t *tree_append_node(tree_node_t *parent, tree_node_t *n)
{
  if(parent == NULL || n == NULL)
    return NULL;

  n->parent = parent;
  n->owner = parent->owner;
  if(n->owner != NULL)
  {
    n->owner->sz++;
  }

  size_t index = parent->len;

  if(tree_children_resize(parent, parent->len+1) <= 0)
    return NULL;

  parent->children[index] = n;
  n->index = index;

  return n;
}

void tree_remove(tree_node_t *node)
{
  if(node == NULL || node->parent == NULL)
    return;

  tree_node_t *p = node->parent;
  if(p->len <= node->index)
    return;

  unsigned int i;
  for(i = node->index+1; i<p->len; i++)
  {
    p->children[i-1] = p->children[i];
  }
  p->len--;
  if(p->len < (p->allocated/2))
    tree_children_resize(p, p->len);
  node->parent = NULL;
  if(node->owner != NULL)
    node->owner->sz--;
  node->owner = NULL;
}

void tree_remove_and_free(tree_node_t *node)
{
  if(node == NULL)
    return;
  tree_remove(node);
  tree_free_children(node);
  tree_clean(node);
}

tree_node_t *tree_find(tree_node_t *parent, void *val)
{
  if(parent == NULL)
    return NULL;
  if(parent->val == val)
    return parent;
  unsigned int i;
  for(i = 0; i<parent->len; i++)
  {
    tree_node_t *res = tree_find(parent->children[i], val);
    if(res != NULL) return res;
  }
  return NULL;
}

tree_node_t *tree_find_custom(tree_node_t *parent, void *val, tree_compare_t compare)
{
  if(parent == NULL)
    return NULL;
  if(compare(parent, val))
    return parent;
  unsigned int i;
  for(i = 0; i<parent->len; i++)
  {
    tree_node_t *res = tree_find(parent->children[i], val);
    if(res != NULL) return res;
  }
  return NULL;
}

void tree_apply(tree_node_t *parent, tree_apply_t apply, void *param)
{
  if(parent == NULL)
    return;
  int r = apply(parent, param);
  if(r == -1)
    return;
  unsigned int i;
  for(i = 0; i<parent->len; i++)
  {
    tree_apply(parent->children[i], apply, param);
  }
}

void tree_free_children(tree_node_t *node)
{
  unsigned int i;
  for(i = 0; i<node->len; i++)
  {
    tree_node_t *n = node->children[i];
    if(n == NULL)
      continue;
    tree_free_children(n);
    tree_clean(n);
  }
  free(node->children);
  node->children = NULL;
  node->allocated = 0;
  node->len = 0;
}

void tree_clean(tree_node_t *node)
{
  if(node == NULL)
    return;
  if(node->flags & TREE_VAL_TO_BE_FREE)
  {
    free(node->val);
  }
  if(node->flags & TREE_NODE_TO_BE_FREE)
  {
    free(node);
  }
}

void tree_empty(tree_t *tree)
{
  tree_free_children(&tree->root);
}

void tree_destroy(tree_t *tree)
{
  tree_empty(tree);
  free(tree);
}

void tree_dump(tree_t *tree)
{
  if(__builtin_expect(tree == NULL, 0))
  {
    return;
  }

  printf("tree_dump: %p, root val=%p\n", tree, tree->root.val);
  printf("tree_dump:  -> [INDEX] (node pointer):(node value)\n");
  tree_dump_node(&tree->root, 1);
}

void tree_dump_node(tree_node_t *node, int spacing)
{
  unsigned int i;
  if(node->children == NULL)
    return;
  for(i = 0; i<node->len; i++)
  {
    tree_node_t *n = node->children[i];
    printf("tree_dump:");
    printf("%*.s", spacing*2, " ");
    printf("-> [%u] %p:%p\n", n->index, n, n->val);
    tree_dump_node((tree_node_t *) n, spacing + 1);
  }
}
