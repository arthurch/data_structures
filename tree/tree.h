#ifndef TREE_H
#define TREE_H

/*
 * tree data structure
 *
 *   Each node is used to store a value
 * as a void pointer.
 *   Each node contains a list of
 * its children nodes, which is stored
 * as a kind of dynamic array which resize
 * when items are added.
 *   A tree structure just contains a root
 * node.
 *
 */

#include <stddef.h> /* needed for size_t */

/* growth ratio. When we need to resize node's children,
 the new allocated size is [needed size]*TREE_GROWTH_RATE */
#define TREE_GROWTH_RATE 1.5

/* indicates if a node need to be freed when it is no longer
  needed */
#define TREE_NODE_TO_BE_FREE 0x1
/* indicates if the value stored by a node was allocated and need
  to be freed when we empty the tree */
#define TREE_VAL_TO_BE_FREE 0x2

/* an actual node */
struct tree_node {
  void *val; /* value set by user */

  /* every other variables should be ignored by user,
    they are handled by tree_*() functions */
  unsigned int index; /* index in parent's children list */
  unsigned int flags;

  struct tree_node *parent;
  struct tree *owner;

  size_t len; /* number of children in the children list */
  size_t allocated; /* allocated size of the children list */
  struct tree_node **children;
};
typedef struct tree_node tree_node_t;

struct tree {
  size_t sz; /* number of elements */
  tree_node_t root;
};
typedef struct tree tree_t;

typedef int(*tree_compare_t)(tree_node_t *n, void *val);
typedef int(*tree_apply_t)(tree_node_t *n, void *param);

tree_t *tree_create(void *rootval); /* create a tree */
/* initalize, or reinitialize a tree.
  /!\ Be careful : the tree_init function empty the tree if its size is not
  equals to 0, so setting tree's size to zero is mandatory if you just created
  it and it is not yet a valid tree (e.g if created as a local variable) */
tree_t *tree_init(tree_t *tree, void *rootval);
tree_node_t *tree_create_node(void *val); /* malloc a node */

/* append a node to the tree, by indicating its parent node.
  tree_append creates a node to store the value. The allocatedval parameter
    should be set to 1 is val needs to be freed when we free the tree */
tree_node_t *tree_append(tree_node_t *parent, void *val, int allocatedval);
tree_node_t *tree_append_node(tree_node_t *parent, tree_node_t *node);

/* remove a node from the tree.
  tree_remove_and_free alost free the node */
void tree_remove(tree_node_t *node);
void tree_remove_and_free(tree_node_t *node);

/* find a node corresponding to a given value, either by doing a simple == or by using
  the function 'compare' given in parameters which should return 1 if value is matching.
  These functions returns the first matching node */
tree_node_t *tree_find(tree_node_t *parent, void *val);
tree_node_t *tree_find_custom(tree_node_t *parent, void *val, tree_compare_t compare);

/* apply a function given in parameters to every nodes (every sub nodes of parent, and
  parent itself) */
void tree_apply(tree_node_t *parent, tree_apply_t apply, void *param);

void tree_free_children(tree_node_t *node); /* free every children and/or values if
                                                necessary (see flags TREE_*_TO_BE_FREE) */
void tree_empty(tree_t *tree); /* empty a tree (equivalent to tree_free_children(&tree.root)) */
void tree_destroy(tree_t *tree); /* empty a tree and free it */
void tree_clean(tree_node_t *node); /* free a node if necessary */

/* these functions dump the nodes as pointers so it is kind of ugly and should only be
  used for debugging purposes */
void tree_dump(tree_t *tree); /* dump a tree */
void tree_dump_node(tree_node_t *n, int spacing); /* dump sub nodes */

#endif
