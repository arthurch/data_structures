#include <stdio.h>
#include <string.h>

#include <tree.h>

int compare_str(tree_node_t *n, void *val)
{
  return strcmp((char *) n->val, (char *) val) == 0;
}

int delete_starting(tree_node_t *n, void *param)
{
  int i;
  char c = *((char *) param);

  if(n->len == 0)
    return 0;
  for(i = n->len-1; i>=0; i--)
  {
    char *s = ((char *) n->children[i]->val);
    if(s[0] == c)
    {
      tree_remove_and_free(n->children[i]);
    }
  }
  return 0;
}

int dump_children(tree_node_t *n, void *param)
{
  printf("children of %s (%p) :", (char *) n->val, n);
  int i = 0;
  for(i=0; i<n->len; i++)
  {
    printf("%p - ", n->children[i]);
  }
  printf("\n");
  return 0;
}

int main2(int argc, char **argv)
{
  printf("trees\n\n");

  tree_t tree;
  tree.sz = 0; /* so tree_init don't try to empty what's not yet a tree */
  tree_init(&tree, "hellos");

  tree_append(&tree.root, "fr", 0);
  tree_node_t *en = tree_append(&tree.root, "eb", 0);
  en->val = "en";

  tree_append(tree.root.children[0], "bonjour", 0);
  tree_append(tree.root.children[0], "bonsoir", 0);
  tree_append(tree.root.children[0], "bonne nuit", 0);

  tree_append(tree.root.children[1], "hello", 0);
  tree_append(tree.root.children[1], "hi", 0);
  tree_append(tree.root.children[1], "good night", 0);

  tree_append(tree.root.children[1], "hello2", 0);
  tree_append(tree.root.children[1], "hi2", 0);
  tree_append(tree.root.children[1], "xgood night2", 0);

  tree_append(tree.root.children[1], "hello3", 0);
  tree_append(tree.root.children[1], "hi3", 0);
  tree_append(tree.root.children[1], "good night3", 0);

  printf("size = %lu\n", tree.sz);

  tree_node_t *hi = tree_find_custom(&tree.root, "hi", compare_str);
  hi->val = "HI";

  char c = 'x';
  tree_apply(&tree.root, delete_starting, &c);

  printf("size = %lu\n", tree.sz);

  tree_dump(&tree);

  tree_empty(&tree);

  return 0;
}
