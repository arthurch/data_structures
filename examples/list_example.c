#include <stdio.h>
#include <string.h>

#include <lists.h>

int compare_alph(void *i1, void *i2)
{
  char c1 = ((char *) i1)[0];
  char c2 = ((char *) i2)[0];
  return c1 < c2;
}

int compare_contains_number(void *item, void *compare_to)
{
  char *s = (char *) item;
  char c;
  unsigned int i;
 
  for(i = 0; i<strlen(s); i++)
  {
    c = s[i];
    if(c >= '0' && c <= '9')
      return 1;
  }
  return 0;
}

int main(int argc, char **argv)
{
  list_t l;
  l.len = 0;
  list_init(&l, 5, 0);
 
  l.items[0] = "ahey0";
  l.items[1] = "to be removed";
  l.items[2] = "ehey2";
  l.items[3] = "dhey3";
  l.items[4] = "chey";

  list_append(&l, "1bonsoir");
  printf("appended at index %u\n", list_append(&l, "bonjour"));

  list_append(&l, "bah");

  list_remove(&l, 1);
  char *s = (char *) list_pop(&l);
  printf("removed: %s\n", s);

  unsigned int i;
  for(i = 0; i<l.len; i++)
  {
    printf("[%u] %s\n", i, (char *) l.items[i]);
  }

  list_sort(&l, compare_alph);
  printf("\nsorted\n");

  for(i = 0; i<l.len; i++)
  {
    printf("[%u] %s\n", i, (char *) l.items[i]);
  }

  printf("\ncontaining numbers :\n");
  list_t *res = list_find_custom(&l, NULL, compare_contains_number);
  for(i = 0; i<res->len; i++)
  {
    printf("[%u] %s\n", i, (char *) l.items[(int) res->items[i]]);
  }

  list_destroy(res);
  list_empty(&l);

  return 0;
}
