#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* this could be wrong (edit it if necessary) */
#include "lists.h"

static int list_resize(list_t *l, size_t newsize)
{
  size_t allocated = l->allocated_len;
  size_t new_allocated;
  size_t allocated_bytes;

  if(allocated >= newsize && newsize >= (allocated >> 1))
  {
    l->len = newsize;
    return l->allocated_len;
  }
  
  new_allocated = (size_t) (newsize + ((size_t) newsize >> 3) + 6) & ~(size_t)3;
  if(newsize - l->len > new_allocated - newsize)
    new_allocated = ((size_t) newsize + 3) & ~(size_t)3;

  allocated_bytes = new_allocated * sizeof(void *);
 
  void **items = (void **) realloc(l->items, allocated_bytes);
  if(items == NULL)
  {
    return -1;
  }

  l->items = items;
  l->allocated_len = new_allocated;
  l->len = newsize;
  return new_allocated;
}

list_t *list_create(size_t len, int valfree)
{
  list_t *l = (list_t *) malloc(sizeof(struct list));
  l->len = 0;
  list_init(l, len, valfree);
  return l;
}

void list_init(list_t *l, size_t len, int valfree)
{
  if(l->len != 0)
  {
    list_empty(l);
  }
  memset(l, 0, sizeof(struct list));

  list_resize(l, len);
  memset(l->items, 0, l->allocated_len*LIST_ITEM_SIZE);

  if(valfree)
    l->flags |= LIST_VAL_TO_BE_FREE;
}

int list_append(list_t *l, void *item)
{
  if(l == NULL)
    return -1;

  size_t index = l->len;
  if(list_resize(l, l->len+1) < 0)
    return -1;
  
  l->items[index] = item;
  return index;
}

void *list_get(list_t *l, int index)
{
  if(index >= (int) l->len || l->items == NULL || l->len == 0)
    return NULL;
  if(index < 0)
    index += (int) l->len;

  return l->items[index];
}

void *list_remove(list_t *l, int index)
{
  if(index >= (int) l->len || l->items == NULL || l->len == 0)
    return NULL;
  if(index < 0)
  {
    index += (int) l->len;
  }

  void *item = l->items[index];
  unsigned int i;
  for(i = index+1; i<l->len; i++)
  {
    l->items[i-1] = l->items[i];
  }
  list_resize(l, l->len-1);

  return item;
}

void *list_pop(list_t *l)
{
  return list_remove(l, -1);
}

/** Quicksort algorithm to sort the list **/

static int partition(list_t *l, int lo, int hi, list_compare_t compare)
{
  void *pivot = l->items[hi];
  void *tmp;

  unsigned int i = lo;
  unsigned int j;
  for(j = lo; j<hi; j++)
  {
    if(compare(l->items[j], pivot))
    {
      tmp = l->items[i];
      l->items[i] = l->items[j];
      l->items[j] = tmp;
      i++;
    }
  }
  tmp = l->items[i];
  l->items[i] = l->items[hi];
  l->items[hi] = tmp;

  return i;
}

static void quicksort(list_t *l, int lo, int hi, list_compare_t compare)
{
  if(lo<hi)
  {
    int pivot = partition(l, lo, hi, compare);
    quicksort(l, lo, pivot-1, compare);
    quicksort(l, pivot+1, hi, compare);
  }
}

void list_sort(list_t *l, list_compare_t compare)
{
  if(l == NULL || l->len <= 1)
    return;
  quicksort(l, 0, l->len-1, compare);
}

/*******************************************/

int list_find_first(list_t *l, void *item)
{
  int i;
  for(i = 0; i<l->len; i++)
  {
    if(l->items[i] == item)
      return i;
  }
  return -1;
}

list_t *list_find_custom(list_t *l, void *item, list_compare_t compare)
{
  list_t *res = list_create(0, 0); 
  int i;
  for(i = 0; i<l->len; i++)
  {
    if(compare(l->items[i], item))
    {
      list_append(res, (void *) (long) i);
    }
  }
  return res;
}

void list_empty(list_t *l)
{
  if(l == NULL || l->allocated_len == 0 || l->items == NULL)
    return;
  if(l->flags & LIST_VAL_TO_BE_FREE)
  {
    unsigned int i;
    for(i = 0; i<l->len; i++)
    {
      free(l->items[i]);
    }
  }

  free(l->items);
  l->allocated_len = 0;
  l->len = 0;
}

void list_destroy(list_t *l)
{
  if(l == NULL) return;
  list_empty(l);
  free(l);
}

