#ifndef LISTS_H
#define LISTS_H

/* for size_t */
#include <stddef.h>

/*
 * list data structure 
 * 
 *   Dynamic arrays storing void
 * pointers.
 *
 */

#define LIST_ITEM_SIZE __SIZEOF_POINTER__

#define LIST_VAL_TO_BE_FREE 1

struct list {
  size_t len; /* number of items */
  size_t allocated_len; /* allocated size of the items
                          array */

  unsigned int flags;

  void **items; /* actual array storing the items */
};
typedef struct list list_t;

/* see list_sort */
typedef int(*list_compare_t)(void *item1, void *item2);

/* create a list of size len,
 the valfree parameters should be set to 1 if items
 need to be freed when we empty the list */
list_t *list_create(size_t len, int valfree);
/* initialize or reinitialize an existing list.
 /!\ Be careful : l->len must be set to 0 if you don't want
 this function to try to empty the list. */
void list_init(list_t *l, size_t len, int valfree);

/* append an element at the end of the list */
int list_append(list_t *l, void *item);
/* return the element at the given index (index can be
 negative, in which case it returns the item at (index + l->len) */
void *list_get(list_t *l, int index);
/* remove (and return) the item at given index */
void *list_remove(list_t *l, int index);
/* remove (and return) the last item */
void *list_pop(list_t *l);

/* sort the list in growing order, the compare function should return
 1 (true) if item1 < item 2.
 It uses a quicksort algorithm. */
void list_sort(list_t *l, list_compare_t compare);

int list_find_first(list_t *l, void *item);
list_t *list_find_custom(list_t *l, void *item, list_compare_t compare);

/* empty the list and free items if the flag LIST_VAL_TO_BE_FREE
 is set */
void list_empty(list_t *l);

/* empty the list and free it */
void list_destroy(list_t *l);

#endif
