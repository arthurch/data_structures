CFLAGS:=-g -Wall -Itree -Ilists
LFLAGS:=

SRCS:=$(wildcard tree/*.c) \
	$(wildcard examples/*.c) \
	$(wildcard lists/*.c)
OBJS:=$(SRCS:.c=.o)

OUTPUT:=datatest

.PHONY: all clean

all: $(OUTPUT)
	@rm $(OBJS)

$(OUTPUT): $(OBJS)
	gcc $(LFLAGS) -o $(OUTPUT) $(OBJS)

%.o: %.c
	gcc $(CFLAGS) -c $< -o $@

clean:
	rm src/*.o
	rm $(OUTPUT)
